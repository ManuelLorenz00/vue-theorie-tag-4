import Vue from 'vue'
import App from './App.vue'
import router from '@/router'
import store from '@/store'
import VueAxios from 'vue-axios'
import axios from 'axios'

axios.interceptors.request.use(request => {
  if(!request.headers) {
    request.headers = {};
  }
  request.headers.Authorization = 'Bearer ' + localStorage.getItem('jwt');
  return request;
});

Vue.use(VueAxios, axios)
Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App),
}).$mount('#app')

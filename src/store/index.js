import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    user: null,
    products: [],
    invoices: []
  },
  mutations: {
    setUser(state, user) {
      state.user = user
    },
    deleteUser(state) {
      state.user = null
    }
  },
  actions: {
    async register(context, register) {
      const response = await Vue.axios.post('https://codersbay.a-scho-wurscht.at/api/auth/register', register);
      localStorage.setItem('jwt', response.data.accessToken);
      context.commit('setUser', response.data.user);
    },
    async login(context, credentails) {
      const response = await Vue.axios.post('https://codersbay.a-scho-wurscht.at/api/auth/login', credentails);
      localStorage.setItem('jwt', response.data.accessToken);
      context.commit('setUser', response.data.user);
    },
    async verifyUser(context) {
      const response = await Vue.axios.get('https://codersbay.a-scho-wurscht.at/api/auth');
      localStorage.setItem('jwt', response.data.accessToken);
      context.commit('setUser', response.data.user);
    }
  }
});

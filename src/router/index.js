import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home'
import Login from '../views/Login'
import UserList from '../views/UserList'
import DashboardLayout from '../layouts/DashboardLayout'
import AuthLayout from '../layouts/AuthLayout'
import Register from '@/views/Register'

Vue.use(VueRouter)

export default new VueRouter({
  routes: [
    {
      path: '/',
      component: DashboardLayout,
      children: [
        {
          path: '',
          component: Home
        },
        {
          path: '/users',
          component: UserList,
        }
      ],
      beforeEnter (to, from, next) {
        if (localStorage.getItem('jwt')) {
          next()
        } else {
          next('/login')
        }
      }
    },
    {
      path: '/login',
      component: AuthLayout,
      children: [
        {
          path: '',
          component: Login
        },
        {
          path: '/register',
          component: Register
        }
      ]
    }
  ]
})
